//
//  ViewController.swift
//  car2secure
//
//  Created by mmpkl04 on 8/30/17.
//  Copyright © 2017 fishy. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase

class ViewController: UIViewController {
    
    @IBOutlet weak var emailInputField: UITextField!
    @IBOutlet weak var passwordInputField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func viewDidAppear(_ animated: Bool) {
        emailInputField.text = ""
        passwordInputField.text = ""
        
        let keyChain = DataService().keyChain
        if keyChain.get("uid") != nil {
            performSegue(withIdentifier: "Car1", sender: nil)
        }
    }
    
    func completeSignIn(id: String) {
        let keyChain = DataService().keyChain
        keyChain.set(id, forKey: "uid")
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func signIn(_ sender: Any) {
        if let email = emailInputField.text, let password = passwordInputField.text {
            Auth.auth().signIn(withEmail: email, password: password) { (user, error) in
                if error == nil {
                    if (email == "happycloud@daimler.com") {
                        self.performSegue(withIdentifier: "Car2", sender: nil)
                    }
                    else {
                        self.performSegue(withIdentifier: "Car1", sender: nil)
                    }
                }
                else {
                    Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
                        if error != nil {
                            let alertController = UIAlertController(title: "Error", message: error?.localizedDescription, preferredStyle: .alert)
                            
                            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                            alertController.addAction(defaultAction)
                            
                            self.present(alertController, animated: true, completion: nil)
                        }
                        else {
                            if (email == "happycloud@daimler.com") {
                                self.performSegue(withIdentifier: "Car2", sender: nil)
                            }
                            else {
                                self.performSegue(withIdentifier: "Car1", sender: nil)
                            }
                        }
                    }
                }
            }
        }
    }


}

