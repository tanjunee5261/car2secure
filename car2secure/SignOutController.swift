//
//  SignOutController.swift
//  car2secure
//
//  Created by mmpkl04 on 9/2/17.
//  Copyright © 2017 fishy. All rights reserved.
//

import UIKit
import Firebase
import KeychainSwift

class SignOutController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    @IBAction func signOut (_ sender: Any) {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        }
        catch let signOutError as NSError {
            print ("Error signing out: %@", signOutError)
        }
        DataService().keyChain.delete("uid")
        dismiss(animated: true, completion: nil)
    }
}
